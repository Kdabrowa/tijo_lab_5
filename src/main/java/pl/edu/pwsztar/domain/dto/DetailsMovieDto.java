package pl.edu.pwsztar.domain.dto;

import java.io.Serializable;

public class DetailsMovieDto implements Serializable {
  private String videoId;
  private String title;
  public DetailsMovieDto() {

  }

  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }
  public String getVideoId() {
    return videoId;
  }
  public void setVideoId(String videoId) {
    this.videoId = videoId;
  }


  @Override
  public String toString() {
    return "CreateMovieDto{" +
        "title='" + title + '\'' +
        ", videoId=" + videoId +
        '}';
  }
}
